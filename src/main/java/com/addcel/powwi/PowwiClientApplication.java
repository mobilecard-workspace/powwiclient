package com.addcel.powwi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PowwiClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PowwiClientApplication.class, args);
	}

}
