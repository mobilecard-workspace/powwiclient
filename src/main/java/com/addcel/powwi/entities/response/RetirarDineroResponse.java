package com.addcel.powwi.entities.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetirarDineroResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoResultado;
	private String mensaje;
	private Long idTransaccion;
	private Integer valorTotalTransaccion;
	private Integer costoTransaccion;
	private Integer costoIVA;
	private Integer cobroGMF;
	private String OTP;
	private String validezOTP;
	private String fecha;
	private Integer idOperacion;
	private String infoDispositivo;
	private String IP;

}
