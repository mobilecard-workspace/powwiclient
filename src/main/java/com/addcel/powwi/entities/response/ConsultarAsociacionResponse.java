package com.addcel.powwi.entities.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarAsociacionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resultado;
	private String mensaje;
	private String idCuenta;
	private String fecha;
	private String idOperacion;

}
