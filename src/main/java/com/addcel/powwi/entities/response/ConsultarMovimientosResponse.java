package com.addcel.powwi.entities.response;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarMovimientosResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoResultado;
	private String mensaje;
	private String fecha;
	private List<Movimientos> movimientos;
	private Integer idOperacion;

}
