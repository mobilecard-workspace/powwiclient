package com.addcel.powwi.entities.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Movimientos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fecha;
	private Integer idOperacion;
	private Integer idMovimiento;
	private String descripcion;
	private String tipoOperacion;
	private String TipoMovimiento;
	private String valor;
	private String saldoInicial;
	private String saldoFinal;
	private String referencia1;
	private String referencia2;
	private String referenciaExterna;

}
