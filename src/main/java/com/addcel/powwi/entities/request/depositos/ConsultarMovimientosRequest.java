package com.addcel.powwi.entities.request.depositos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarMovimientosRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tokenConcenio;
	private String tokenConsulta;
	private String huellaDispositivo;
	private Integer idCuenta;
	private String fechaInicio;
	private String fechaFin;

}
