package com.addcel.powwi.entities.request.depositos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenerarTokenTransaccionalRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tokenConvenio;
	private Integer tipoValidacion;
	private Integer tipoTransaccion;
	private Long idCuenta;
	private Integer celular;
	private String otp;
	private String idEnrolamiento;
	private String idValidacion;
	private String scoreValidacion;
	private String idAutenticacionAliado;

}
