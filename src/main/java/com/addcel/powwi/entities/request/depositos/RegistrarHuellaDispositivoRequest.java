package com.addcel.powwi.entities.request.depositos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrarHuellaDispositivoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tokenConvenio;
	private TokenTransaccional tokenTransaccional;
	private Long idCuenta;
	private String nuevaHuella;

}
