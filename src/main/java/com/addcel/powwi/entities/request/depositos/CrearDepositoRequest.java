package com.addcel.powwi.entities.request.depositos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CrearDepositoRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tokenConvenio;
	private TokenTransaccional tokenTransaccional;
	private String huellaDeposito;
	private String tipoDocumento;
	private Integer numeroDocumento;
	private String fechaExpedicion;
	private String celular;
	private String fechaNacimiento;
	private String correoElectronico;
	private char sexo;
	private Boolean checkDatos;
	private Boolean checkProducto;
	private Boolean checkHabbeasData;
}
