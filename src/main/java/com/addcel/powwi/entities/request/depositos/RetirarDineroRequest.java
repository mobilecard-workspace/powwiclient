package com.addcel.powwi.entities.request.depositos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetirarDineroRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tokenCoinvenio;
	private String tokenTransaccional;
	private String huellaDispositivo;
	private Integer canalRetiro;
	private Long idCuenta;
	private Integer monto;
	private String infoDispositivo;
	private String ip;

}
