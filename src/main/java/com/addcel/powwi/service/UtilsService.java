package com.addcel.powwi.service;

import com.addcel.powwi.entities.request.AuthRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarMovimientosRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarSaldoRequest;
import com.addcel.powwi.entities.request.depositos.GenerarTokenTransaccionalRequest;
import com.addcel.powwi.entities.response.AuthResponse;
import com.addcel.powwi.entities.response.ConsultarMovimientosResponse;
import com.addcel.powwi.entities.response.ConsultarSaldoResponse;
import com.addcel.powwi.entities.response.GenerarTokenTransaccionalResponse;

public interface UtilsService {

	AuthResponse autenticar(AuthRequest request) throws Exception;
	GenerarTokenTransaccionalResponse generarTokenTransaccional(GenerarTokenTransaccionalRequest request) throws Exception;
	ConsultarSaldoResponse consultarSaldo(ConsultarSaldoRequest request) throws Exception;
	ConsultarMovimientosResponse consultarMovimientos(ConsultarMovimientosRequest request) throws Exception;
}
