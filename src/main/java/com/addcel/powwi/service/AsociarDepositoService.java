package com.addcel.powwi.service;

import com.addcel.powwi.entities.request.depositos.AsociarDepositoRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarAsociacionRequest;
import com.addcel.powwi.entities.response.AsociarDepositoResponse;
import com.addcel.powwi.entities.response.ConsultarAsociacionResponse;

public interface AsociarDepositoService {

	AsociarDepositoResponse asociarDeposito(AsociarDepositoRequest request) throws Exception;
	ConsultarAsociacionResponse consultarAsociacion(ConsultarAsociacionRequest request) throws Exception;
}
