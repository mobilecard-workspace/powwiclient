package com.addcel.powwi.service;

import com.addcel.powwi.entities.request.depositos.CrearDepositoRequest;
import com.addcel.powwi.entities.request.depositos.GenerarOtpRequest;
import com.addcel.powwi.entities.response.CrearDepositoResponse;
import com.addcel.powwi.entities.response.GenerarOtpRespone;

public interface CrearDepositoService {

	GenerarOtpRespone generarOtp(GenerarOtpRequest request) throws Exception;
	CrearDepositoResponse crearDeposito(CrearDepositoRequest request) throws Exception;
}
