package com.addcel.powwi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.powwi.client.AwsGenericClient;
import com.addcel.powwi.entities.request.depositos.CrearDepositoRequest;
import com.addcel.powwi.entities.request.depositos.GenerarOtpRequest;
import com.addcel.powwi.entities.response.CrearDepositoResponse;
import com.addcel.powwi.entities.response.GenerarOtpRespone;
import com.addcel.powwi.service.CrearDepositoService;
import com.addcel.powwi.utils.AppUtils;
import com.addcel.powwi.utils.CrearDepositoUtils;
import com.addcel.powwi.utils.GenerarOtpUtils;
import com.google.gson.Gson;

import ca.ryangreen.apigateway.generic.GenericApiGatewayException;
import ca.ryangreen.apigateway.generic.GenericApiGatewayResponse;

@Service
public class CrearDepositoServiceImpl implements CrearDepositoService {
	
	@Autowired
	private AwsGenericClient awsClient;

	private static final Logger LOGGER = LoggerFactory.getLogger(CrearDepositoServiceImpl.class);
	private Gson GSON = new Gson();
	
	@Override
	public GenerarOtpRespone generarOtp(GenerarOtpRequest request) throws Exception {
		GenerarOtpRespone response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la generación de un OTP {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.GOT);
			response = GSON.fromJson(apiResponse.getBody(), GenerarOtpRespone.class);
			LOGGER.debug("Respuesta del servicio de generacion de OTP: {}",response);
			switch (response.getCodigoResultado()) {
			case GenerarOtpUtils.TRANSACCION_EXITOSA:
				return response;
			case GenerarOtpUtils.CELULAR_ASOCIADO:
				throw new Exception("Celular Asociado a un producto.");
			case GenerarOtpUtils.NUMERO_CELULAR_INVALIDO:
				throw new Exception("Estructura Número de Celular no Válida.");
			case GenerarOtpUtils.TIPO_TRANSACCION_INVALIDA:
				throw new Exception("Tipo Transacción no Válida.");
			case GenerarOtpUtils.TOKEN_INVALIDO:
				throw new Exception("Token Convenio no Válido.");
			case GenerarOtpUtils.IDCUENTA_INVALIDO:
				throw new Exception("ID Cuenta no Válido.");
			case GenerarOtpUtils.CANAL_INVALIDO:
				throw new Exception("Canal no Válido.");
			case GenerarOtpUtils.CODGIO_TRANS_INVALIDO:
				throw new Exception("Código Transacción no Válido.");
			case GenerarOtpUtils.DEMASIADOS_INTENTOS:
				throw new Exception("Demasiados Intentos para Generar OTP.");
			case GenerarOtpUtils.ERROR_GRAVE:
				throw new Exception("Error Grave en la Capa de Negocio (Perdida de Conexión o Timeout del Servicio).");
			default:
				throw new Exception("Error Interno del servidor.");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error al generar un OTP: {}",e.getMessage());
			throw new Exception("Error al generar un OTP: "+e.getMessage());
		}
	}

	@Override
	public CrearDepositoResponse crearDeposito(CrearDepositoRequest request) throws Exception {
		CrearDepositoResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la creacion de un deposito {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.CDE);
			response = GSON.fromJson(apiResponse.getBody(), CrearDepositoResponse.class);
			LOGGER.debug("Respuesta del servicio de creacion de deposito: {}",response);
			switch (response.getCodigoResultado()) {
			case CrearDepositoUtils.TRANSACCION_EXITOSA:
				return response;
			case CrearDepositoUtils.CHECKS_NO_ACEPTADOS:
				throw new Exception("Checks no Aceptados.");
			case CrearDepositoUtils.TRANSACCION_EXITOSA_PENDIENTE:
				return response;
			case CrearDepositoUtils.TIPO_DOCUMENTO_INVALIDO:
				throw new Exception("Tipo de Documento no Válido.");
			case CrearDepositoUtils.NUM_DOCUMENTO_INVALIDO:
				throw new Exception("Número de Documento no Válido.");
			case CrearDepositoUtils.CORREO_INVALIDO:
				throw new Exception("Estructura del Correo Electrónico no Válida.");
			case CrearDepositoUtils.FECHA_EXPEDICION_INVALIDA:
				throw new Exception("Fecha de Expedición del Documento no Válida.");
			case CrearDepositoUtils.SEXO_INVALIDO:
				throw new Exception("Parámetro del Sexo no es Válido.");
			case CrearDepositoUtils.CELULAR_INVALIDO:
				throw new Exception("Celular no Válido.");
			case CrearDepositoUtils.EMAIL_INVALIDO:
				throw new Exception("Email no Válido.");
			case CrearDepositoUtils.FECHA_NACIMIENTO_INVALIDA:
				throw new Exception("Fecha de Nacimiento no Válida.");
			case CrearDepositoUtils.TIPO_SIS_INVALIDO:
				throw new Exception("Tipo Sistema no Válido.");
			case CrearDepositoUtils.NOMBRE_DISPOSITIVO_INVALIDO:
				throw new Exception("Nombre de Dispositivo no Válido.");
			case CrearDepositoUtils.IP_INVALIDA:
				throw new Exception("Ip no Válida.");
			case CrearDepositoUtils.HUELLA_DISPOSITIVO_INVALIDA:
				throw new Exception("Huella de Dispositivo no Válida.");
			case CrearDepositoUtils.TOKEN_CONVENIO_INVALIDO:
				throw new Exception("Token Convenio no Válido.");
			case CrearDepositoUtils.INFORMACION_INCORRECTA_DOCUMENTO:
				throw new Exception("Información Incorrecta Documento de Identificación.");
			case CrearDepositoUtils.TOKEN_TRANSACCIONAL_INVALIDO:
				throw new Exception("Token Transaccional no Válido.");
			case CrearDepositoUtils.TOKEN_TRANS_EXPIRO:
				throw new Exception("El Token Transaccional Expiró.");
			case CrearDepositoUtils.ERROR_GRAVE:
				throw new Exception("Error Grave en la Capa de Negocio.");
			default:
				throw new Exception("Error Interno del Servidor.");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error al crear un deposito: {}",e.getMessage());
			throw new Exception("Error al crear un deposito: "+e.getMessage());
		}
	}

}
