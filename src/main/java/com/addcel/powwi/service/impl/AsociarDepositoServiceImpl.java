package com.addcel.powwi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.powwi.client.AwsGenericClient;
import com.addcel.powwi.entities.request.depositos.AsociarDepositoRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarAsociacionRequest;
import com.addcel.powwi.entities.response.AsociarDepositoResponse;
import com.addcel.powwi.entities.response.ConsultarAsociacionResponse;
import com.addcel.powwi.service.AsociarDepositoService;
import com.addcel.powwi.utils.AppUtils;
import com.addcel.powwi.utils.AsociarDepositoUtils;
import com.addcel.powwi.utils.ConsultarAsociacionUtils;
import com.google.gson.Gson;

import ca.ryangreen.apigateway.generic.GenericApiGatewayException;
import ca.ryangreen.apigateway.generic.GenericApiGatewayResponse;

@Service
public class AsociarDepositoServiceImpl implements AsociarDepositoService {
	
	@Autowired
	private AwsGenericClient awsClient;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AsociarDepositoServiceImpl.class);
	private Gson GSON = new Gson();

	@Override
	public AsociarDepositoResponse asociarDeposito(AsociarDepositoRequest request) throws Exception {
		AsociarDepositoResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la asociacion de deposito {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request) , AppUtils.ADEPOSITO);
			response = GSON.fromJson(apiResponse.getBody(), AsociarDepositoResponse.class);
			LOGGER.debug("Respuesta del servicio de asociacion de deposito: {}",response);
			switch (response.getCodigoResultado()) {
			case AsociarDepositoUtils.TRANSACCION_EXITOSA:
				return response;
			case AsociarDepositoUtils.CELULAR_INVALIDO:
				throw new Exception("Estructura del Celular no Válida.");
			case AsociarDepositoUtils.HUELLA_DISPOSITIVO_INVALIDA:
				throw new Exception("Huella de Dispositivo no Válida.");
			case AsociarDepositoUtils.NUM_DOCUMENTO_INVALIDO:
				throw new Exception("Número de Documento no Válido.");
			case AsociarDepositoUtils.TIPODOCUMENTO_INVALIDO:
				throw new Exception("Tipo de Documento no Válido.");
			case AsociarDepositoUtils.TOKEN_CONVENIO_INVALIDO:
				throw new Exception("Token Convenio no Válido.");
			case AsociarDepositoUtils.TOKEN_TRANSACCIONAL_INVALIDO:
				throw new Exception("Token Transaccion no Válido.");
			default:
				throw new Exception("Error interno del Servidor");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error al asociar un deposito {}",e.getMessage());
			throw new Exception("Error al asociar un deposito "+e.getMessage());
		}
	}

	@Override
	public ConsultarAsociacionResponse consultarAsociacion(ConsultarAsociacionRequest request) throws Exception {
		ConsultarAsociacionResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la consulta de la asociacion de un deposito {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.CAD);
			String body = apiResponse.getBody();
			response = GSON.fromJson(body, ConsultarAsociacionResponse.class);
			LOGGER.debug("Respuesta del servicio de consulta de la asociacion: {}",response);
			switch (response.getResultado()) {
			case ConsultarAsociacionUtils.TRANSACCION_EXITOSA:
				return response;
			case ConsultarAsociacionUtils.ASOCIACION_PENDIENTE:
				throw new Exception("Asociación Pendiente por ser Aceptada por el Cliente.");
			case ConsultarAsociacionUtils.ASOCIACION_RECHAZADA:
				throw new Exception("Asociación Rechazada por el cliente.");
			case ConsultarAsociacionUtils.CELULAR_INVALIDO:
				throw new Exception("Estructura de Celular no Válida.");
			case ConsultarAsociacionUtils.TOKEN_CONVENIO_INVALIDO:
				throw new Exception("Token Convenio no Válido.");
			default:
				throw new Exception("Error Interno del Servidor");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error al consultar la asociacion del deposito {}", e.getMessage());
			throw new Exception("Error al consultar la asociacion del deposito "+e.getMessage());
		}
	}

}
