package com.addcel.powwi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.powwi.client.AwsGenericClient;
import com.addcel.powwi.entities.request.AuthRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarMovimientosRequest;
import com.addcel.powwi.entities.request.depositos.ConsultarSaldoRequest;
import com.addcel.powwi.entities.request.depositos.GenerarTokenTransaccionalRequest;
import com.addcel.powwi.entities.response.AuthResponse;
import com.addcel.powwi.entities.response.ConsultarMovimientosResponse;
import com.addcel.powwi.entities.response.ConsultarSaldoResponse;
import com.addcel.powwi.entities.response.GenerarTokenTransaccionalResponse;
import com.addcel.powwi.service.UtilsService;
import com.addcel.powwi.utils.AppUtils;
import com.addcel.powwi.utils.AuthUtils;
import com.addcel.powwi.utils.ConsultarMovimientosUtils;
import com.addcel.powwi.utils.ConsultarSaldoUtils;
import com.addcel.powwi.utils.GenerarTokenTransaccionalUtils;
import com.google.gson.Gson;

import ca.ryangreen.apigateway.generic.GenericApiGatewayException;
import ca.ryangreen.apigateway.generic.GenericApiGatewayResponse;

@Service
public class UtilsServiceImpl implements UtilsService {
	
	@Autowired
	private AwsGenericClient awsClient;

	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsServiceImpl.class);
	private Gson GSON = new Gson();
	
	@Override
	public AuthResponse autenticar(AuthRequest request) throws Exception {
		AuthResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando el proceso de autenticacion {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.AUT);
			response = GSON.fromJson(apiResponse.getBody(), AuthResponse.class);
			LOGGER.debug("Respuesta del servicio de autenticacion: {}",response);
			if (response.getCodigoResultado().equals(AuthUtils.TRANSACCION_EXITOSA))
				return response;
			else if(response.getCodigoResultado().equals(AuthUtils.USUARIO_BLOQUEADO))
				throw new Exception("Usuario Bloqueado");
			else
				throw new Exception("Credenciales No Válidas");
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error en el servicio de autenticacion: {}",e.getMessage());
			throw new Exception("Error en el servicio de autenticacion: "+e.getMessage());
		}
	}

	@Override
	public GenerarTokenTransaccionalResponse generarTokenTransaccional(GenerarTokenTransaccionalRequest request) throws Exception {
		GenerarTokenTransaccionalResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la generacion de un token transaccional {}",request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.GTT);
			response = GSON.fromJson(apiResponse.getBody(), GenerarTokenTransaccionalResponse.class);
			LOGGER.debug("Respuesta del servicio de generacion de token transaccional: {}",response);
			switch (response.getCodigoResultado()) {
			case GenerarTokenTransaccionalUtils.TRANSACCION_EXITOSA:
				return response;
			case GenerarTokenTransaccionalUtils.CELULAR_NO_EXISTE:
				throw new Exception("El Número Celular No Existe.");
			case GenerarTokenTransaccionalUtils.ID_AUTENTICACION_INVALIDO:
				throw new Exception("ID de Autenticacion no Válido.");
			case GenerarTokenTransaccionalUtils.ID_CUENTA_INEXISTENTE:
				throw new Exception("El ID de Cuenta no Existe.");
			case GenerarTokenTransaccionalUtils.ID_ENROLAMIENTO_INVALIDO:
				throw new Exception("ID de Enrolamiento no Válido.");
			case GenerarTokenTransaccionalUtils.ID_VALIDACION_BIOMETRICA:
				throw new Exception("ID de Validación Biométrica no Válido.");
			case GenerarTokenTransaccionalUtils.OTP_INVALIDA:
				throw new Exception("OTP no Válida.");
			case GenerarTokenTransaccionalUtils.SCORE_VALIDACION_INVALIDO:
				throw new Exception("Score de Validación no Válido.");
			case GenerarTokenTransaccionalUtils.TIPO_VALIDACION_INVALIDA:
				throw new Exception("Tipo de Validación no Válida.");
			case GenerarTokenTransaccionalUtils.TOKEN_CONVENIO_INVALIDO:
				throw new Exception("Token convenio no Válido.");
			default:
				throw new Exception("Error Interno del servidor.");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error en el servicio de la generacion del token transaccional: {}",e.getMessage());
			throw new Exception("Error en el servicio de la generacion del token transaccional: "+e.getMessage());
		}
	}

	@Override
	public ConsultarSaldoResponse consultarSaldo(ConsultarSaldoRequest request) throws Exception {
		ConsultarSaldoResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la consulta de saldo {}",request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.CSA);
			response = GSON.fromJson(apiResponse.getBody(), ConsultarSaldoResponse.class);
			LOGGER.debug("Respuesta de Powwi:{}",response);
			switch (response.getCodigoResultado()) {
			case ConsultarSaldoUtils.TRANSACCION_EXITOSA:
				return response;
			case ConsultarSaldoUtils.CELULAR_INVALIDO:
				throw new Exception("Celular no Válido.");
			case ConsultarSaldoUtils.CUENTA_NO_RELACIONADA_CON_CELULAR:
				throw new Exception("La Cuenta no Tiene Relación con el Número Celular Introducido.");
			case ConsultarSaldoUtils.FORMATO_CELULAR_INVALIDO:
				throw new Exception("Formato Celular no Válido.");
			case ConsultarSaldoUtils.HUELLA_INVALIDA:
				throw new Exception("Huella del Dispositivo no es Válida.");
			case ConsultarSaldoUtils.ID_CUENTA_INVALIDO:
				throw new Exception("ID de Cuenta no Válidos");
			default:
				throw new Exception("Error Interno del Servidor");
			}
		} catch (GenericApiGatewayException e) {
			LOGGER.error("Error en el servicio de consulta de saldo: {}",e.getMessage());
			throw new Exception("Error en el servicio de consulta de saldo: "+e.getMessage());
		}
	}

	@Override
	public ConsultarMovimientosResponse consultarMovimientos(ConsultarMovimientosRequest request) throws Exception {
		ConsultarMovimientosResponse response = null;
		GenericApiGatewayResponse apiResponse = null;
		try {
			LOGGER.debug("Iniciando la consulta de movimientos {}", request);
			apiResponse = awsClient.consumeAwsService(GSON.toJson(request), AppUtils.CMV);
			response = GSON.fromJson(apiResponse.getBody(), ConsultarMovimientosResponse.class);
			LOGGER.debug("Respouesta de POWWI: {}",response);
			switch (response.getCodigoResultado()) {
			case ConsultarMovimientosUtils.TRANSACCION_EXITOSA:
				return response;
			default:
				throw new Exception("Error Interno del Servidor");
			}
		} catch (Exception e) {
			LOGGER.error("Error en el servicio de consulta de movimientos: {}",e.getMessage());
			throw new Exception("Error en el servicio de consulta de movimientos: "+e.getMessage());
		}
	}

}
