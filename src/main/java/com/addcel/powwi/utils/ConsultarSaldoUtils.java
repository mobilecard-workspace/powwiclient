package com.addcel.powwi.utils;

public class ConsultarSaldoUtils {

	public static final String TRANSACCION_EXITOSA = "CSA_200";
	public static final String FORMATO_CELULAR_INVALIDO = "CSA_404";
	public static final String ID_CUENTA_INVALIDO = "CSA_503";
	public static final String CELULAR_INVALIDO = "CSA_504";
	public static final String TOKEN_CONSULTA_INVALIDO = "CSA_509";
	public static final String TOKEN_CONVENIO_INVALIDO = "CSA_500";
	public static final String CUENTA_NO_RELACIONADA_CON_CELULAR = "CSA_525";
	public static final String HUELLA_INVALIDA = "CSA_502";
	public static final String TOKEN_EXPIRADO = "CSA_512";
}
