package com.addcel.powwi.utils;

public class AppUtils {

	public static final String AUT = "au/autenticacionconvenio?";
	public static final String ADE = "MisPesosAPI/ade/acreditarDeposito";
	public static final String CDE = "MisPesosAPI/cde/crearDeposito";
	public static final String GOT = "got/generarotp?";
	public static final String GTT = "gtt/generartokentransacciona";
	public static final String CSA = "MisPesosAPI/csa/consultarSaldo";
	public static final String RHU = "MisPesosAPI/rhu/registrarHuellaDispositivo";
	public static final String RDI = "MisPesosAPI/rdi/retirarDinero";
	public static final String OTC = "MisPesosAPI/otc/obtenerTokenConsulta";
	public static final String DDE = "MisPesosAPI/dde/debitarDeposito";
	public static final String TDE = "MisPesosAPI/tde/transferirDeposito";
	public static final String RTR = "MisPesosAPI/rtr/reversarTransaccion";
	public static final String CER = "MisPesosAPI/cmv/ConsultarEstadoRetiro";
	public static final String CRD = "MisPesosAPI/ctt/cancelarRetiroDinero";
	public static final String CMV = "MisPesosAPI/cmv/consultarMovimientos";
	public static final String CCT = "MisPesosAPI/cct/consultarCostoTransaccion";
	public static final String CCO = "";
	public static final String DSD = "";
	public static final String ADEPOSITO = "";
	public static final String CAD = "";
	public static final String CMO = "";
}
