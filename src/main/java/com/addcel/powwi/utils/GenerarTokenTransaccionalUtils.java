package com.addcel.powwi.utils;

public class GenerarTokenTransaccionalUtils {

	public static final String TRANSACCION_EXITOSA = "GTT_200";
	public static final String TOKEN_CONVENIO_INVALIDO = "GTT_500";
	public static final String TIPO_VALIDACION_INVALIDA = "GTT_501";
	public static final String ID_CUENTA_INEXISTENTE = "GTT_502";
	public static final String CELULAR_NO_EXISTE = "GTT_503";
	public static final String OTP_INVALIDA = "GTT_504";
	public static final String ID_ENROLAMIENTO_INVALIDO = "GTT_505";
	public static final String ID_VALIDACION_BIOMETRICA = "GTT_506";
	public static final String SCORE_VALIDACION_INVALIDO = "GTT_507";
	public static final String ID_AUTENTICACION_INVALIDO = "GTT_508";
}
