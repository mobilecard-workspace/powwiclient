package com.addcel.powwi.utils;

public class ReservarTransaccionUtils {

	public static final String TRANSACCION_EXITOSA = "RTR_200";
	public static final String TOKEN_CONVENIO_INVALIDO = "RTR_500";
	public static final String HUELLA_DISPOSITIVO_INVALIDA = "RTR_501";
	public static final String TOKEN_INVALIDO = "RTR_502";
	public static final String ID_CUENTA_INVALIDO = "RTR_503";
}
