package com.addcel.powwi.utils;

public class CrearDepositoUtils {

	//Codigos Resultado
	public static final String TRANSACCION_EXITOSA = "CDE_200";
	public static final String CHECKS_NO_ACEPTADOS = "CDE_406";
	public static final String TRANSACCION_EXITOSA_PENDIENTE = "CDE_201";
	public static final String TIPO_DOCUMENTO_INVALIDO = "CDE_401";
	public static final String NUM_DOCUMENTO_INVALIDO = "CDE_402";
	public static final String CORREO_INVALIDO = "CDE_403";
	public static final String SEXO_INVALIDO = "CDE_404";
	public static final String FECHA_EXPEDICION_INVALIDA = "CDE_405";
	public static final String CELULAR_INVALIDO = "CDE_407";
	public static final String EMAIL_INVALIDO = "CDE_408";
	public static final String FECHA_NACIMIENTO_INVALIDA = "CDE_410";
	public static final String TIPO_SIS_INVALIDO = "CDE_412";
	public static final String NOMBRE_DISPOSITIVO_INVALIDO = "CDE_413";
	public static final String IP_INVALIDA = " CDE_414";
	public static final String HUELLA_DISPOSITIVO_INVALIDA = "CDE_415";
	public static final String TOKEN_CONVENIO_INVALIDO = "CDE_500";
	public static final String INFORMACION_INCORRECTA_DOCUMENTO = "CDE_502";
	public static final String TOKEN_TRANSACCIONAL_INVALIDO = "CDE_509";
	public static final String TOKEN_TRANS_EXPIRO = "CDE_512";
	public static final String ERROR_GRAVE = "CDE_514";
}
