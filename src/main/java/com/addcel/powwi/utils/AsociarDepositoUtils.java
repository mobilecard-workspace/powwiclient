package com.addcel.powwi.utils;

public class AsociarDepositoUtils {

	//Codigos Resultado
	public static final String TRANSACCION_EXITOSA = "ADE_200";
	public static final String TIPODOCUMENTO_INVALIDO = "ADE_401";
	public static final String NUM_DOCUMENTO_INVALIDO = "ADE_402";
	public static final String CELULAR_INVALIDO = "ADE_403";
	public static final String HUELLA_DISPOSITIVO_INVALIDA = "ADE_404";
	public static final String TOKEN_CONVENIO_INVALIDO = "ADE_500";
	public static final String TOKEN_TRANSACCIONAL_INVALIDO = "ADE_501";
}
