package com.addcel.powwi.utils;

public class RegistrarHuellaUtils {

	public static final String TRANSACCION_EXITOSA = "RHU_200";
	public static final String ID_CUENTA_INVALIDO = "RHU_401";
	public static final String ESTRUCTURA_HUELLA_INVALIDA = "RHU_402";
	public static final String TOKEN_CONVENIO_INVALIDO = "RHU_500";
	public static final String TOKEN_TRANSACCIONAL_INVALIDO = "RHU_501";
}
