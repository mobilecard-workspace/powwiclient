package com.addcel.powwi.utils;

public class RetirarDineroUtils {

	public static final String TRANSACCION_EXITOSA = "RDI_200";
	public static final String TOKEN_CONVENIO_INVALIDO = "RDI_500";
	public static final String TOKEN_TRANSACCIONAL_INVALIDO = "RDI_501";
	public static final String HUELLA_DISPOSITIVO_INVALIDA = "RDI_502";
	public static final String ID_CUENTA_INVALIDO = "RDI_503";
	public static final String LIMITES_SOBREPASADOS = "RDI_504";
	public static final String CLIENTE_SIN_RETIRO_PROGRAMADO = "RDI_505";
}
