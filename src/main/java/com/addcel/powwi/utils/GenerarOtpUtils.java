package com.addcel.powwi.utils;

public class GenerarOtpUtils {

	public static final String TRANSACCION_EXITOSA = "GOT_200";
	public static final String NUMERO_CELULAR_INVALIDO = "GOT_401";
	public static final String TOKEN_INVALIDO = "GOT_500";
	public static final String TIPO_TRANSACCION_INVALIDA = "GOT_501";
	public static final String CELULAR_ASOCIADO = "GOT_502";
	public static final String CANAL_INVALIDO = "GOT_503";
	public static final String IDCUENTA_INVALIDO = "GOT_504";
	public static final String CODGIO_TRANS_INVALIDO = "GOT_507";
	public static final String ERROR_GRAVE = "GOT_508";
	public static final String DEMASIADOS_INTENTOS = "GOT_544";
}
