package com.addcel.powwi.utils;

public class DesasociarDepositoUtils {

	//CodigoResultado
	public static final String TRANSACCION_EXITOSA = "DSD_200";
	public static final String TOKEN_CONVENIO_INVALIDO = "DSD_500";
	public static final String IDCUENTA_INVALIDO = "DSD_501";
	public static final String SALDO_DEPOSITO_MAYORCERO = "DSD_502";
	public static final String IDCUENTA_NO_RELACION_CELULAR = "DSD_503";
}
