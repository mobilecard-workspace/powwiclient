package com.addcel.powwi.utils;

public class ConsultarMovimientosUtils {

	public static final String TRANSACCION_EXITOSA = "CMO_200";
	public static final String IP_INVALIDA = "CMO_402";
	public static final String FORMATO_FECHA_INVALIDO = "CMO_405";
	public static final String TOKEN_CONVENIO_INVALIDO = "CMO_500";
	public static final String CUENTA_INVALIDA = "CMO_503";
	public static final String TOKEN_CONSULTA_INVALIDO = "CMO_509";
	public static final String TOKEN_CONSULTA_VENCIDO = "CMO_512";
	public static final String HUELLA_DISPOSITIVO_INVALIDA = "CMO_502";
}
