package com.addcel.powwi.client;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.http.HttpMethodName;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import ca.ryangreen.apigateway.generic.GenericApiGatewayClient;
import ca.ryangreen.apigateway.generic.GenericApiGatewayClientBuilder;
import ca.ryangreen.apigateway.generic.GenericApiGatewayException;
import ca.ryangreen.apigateway.generic.GenericApiGatewayRequestBuilder;
import ca.ryangreen.apigateway.generic.GenericApiGatewayResponse;

@Service
public class AwsGenericClient {
	
	@Value("${aws.host.qa}")
	private String host;
	@Value("${aws.api.key}")
	private String apiKey;
	@Value("${aws.region}")
	private String region;
	@Value("${aws.service.name}")
	private String serviceName;
	@Value("${aws.access.key}")
	private String accessKey;
	@Value("${aws.secret.key}")
	private String secretKey;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AwsGenericClient.class);
	
	private final String endPoint = "https://"+apiKey+serviceName+region+host;

	public GenericApiGatewayResponse consumeAwsService(String body, String path) throws Exception {
		GenericApiGatewayResponse response = null;
		
		LOGGER.debug("Body to send: {}", body);
		
		GenericApiGatewayClient client = new GenericApiGatewayClientBuilder()
				.withClientConfiguration(new ClientConfiguration())
				.withCredentials(new EnvironmentVariableCredentialsProvider())
				.withEndpoint(endPoint)
				.withRegion(Region.getRegion(Regions.fromName(region)))
				.withApiKey(apiKey)
				.build();
		
		LOGGER.debug("Se generó el cliente AWS");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		
		try {
			response = client.execute(
					new GenericApiGatewayRequestBuilder()
						.withBody(new ByteArrayInputStream(body.getBytes()))
						.withHttpMethod(HttpMethodName.POST)
						.withHeaders(headers)
						.withResourcePath(path).build());
			
			LOGGER.debug("Response code: {}",response.getHttpResponse().getStatusCode());
			
			
		} catch (GenericApiGatewayException e) {
			LOGGER.error("El cliente lanzó una excepcion con el mensaje: {}, y el código de estatus: {}", e.getMessage(), e.getStatusCode());
			throw new GenericApiGatewayException("El cliente lanzó una excepcion con el mensaje: "+e.getMessage());
		}
		return response;
	}
}
