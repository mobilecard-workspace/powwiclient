package com.addcel.powwi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.powwi.entities.ResponseException;

@RestController
public class PowwiController {
	
	private static final Logger LOG = LoggerFactory.getLogger(PowwiController.class);
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseException> handleException(Exception e) {
		int code = -1;
		String message = e.getMessage();
		LOG.error("Error al consumir servicio: "+e.getLocalizedMessage());
        e.printStackTrace();
        return new ResponseEntity<ResponseException>(new ResponseException(message, code), HttpStatus.OK);
    }

}
